package de.ship.clip.common;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser;
import org.springframework.stereotype.Component;

import de.ship.clip.lib.common.KeycloakInformationProvider;

/**
 * 
 * @author henkej
 * 
 */
@Component
public class KeycloakInformation extends KeycloakInformationProvider {
	
	@Value("${spring.security.oauth2.client.provider.keycloak.authorization-uri}")
	private String url;
	
	@Value("${spring.security.oauth2.client.provider.keycloak.issuer-uri}")
	private String issuerUri;
	
	@Override
	public String getKeycloakUserUrl() {
		return super.getKeycloakUserUrl(issuerUri);
	}

	public String getKeycloakLogoutUrl() {
		return super.getKeycloakLogoutUrl(url, "ship");
	}
	
	public String getKeycloakId() {
		SecurityContext context = SecurityContextHolder.getContext();
		if (context != null) {
			Authentication authentication = context.getAuthentication();
			if (authentication != null) {
				DefaultOidcUser user = (DefaultOidcUser) authentication.getPrincipal();
				if (user != null) {
					return user.getName();
				}
			}
		}
		return null;
	}
}
