package de.ship.clip.common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.info.BuildProperties;
import org.springframework.stereotype.Component;

import de.ship.clip.lib.common.BuildInformationProvider;

/**
 * 
 * @author henkej
 *
 */
@Component
public class BuildInformation extends BuildInformationProvider {
	@Value("${server.servlet.context-path:}")
	private String contextPath;

	@Autowired(required = false)
	private BuildProperties buildProperties;

	@Override
	public String getContextPath() {
		return getContextPath(contextPath);
	}

	@Override
	public String getVersion() {
		return getVersion(buildProperties);
	}
}
