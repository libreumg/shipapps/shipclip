package de.ship.clip.modules.profile;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;

import de.ship.clip.common.KeycloakInformation;
import de.ship.clip.lib.common.ProfileHelper;
import de.ship.clip.lib.profile.model.ProfileBean;

/**
 * 
 * @author henkej
 * 
 */
public abstract class ProfileController {

	@Autowired
	private KeycloakInformation keycloak;

	@Autowired
	private MyProfileRepository repository;

	/**
	 * get the profile of the current user
	 * 
	 * @return the profile
	 */
	@ModelAttribute(ProfileHelper.PROFILE_BEAN_NAME)
	public ProfileBean getProfile() {
		return repository.getProfile(keycloak.getKeycloakId());
	}

}
