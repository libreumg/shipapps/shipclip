package de.ship.clip.modules.profile;

import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import de.ship.clip.lib.profile.ProfileRepository;
import de.ship.clip.lib.profile.model.ProfileBean;

/**
 * 
 * @author henkej
 * 
 */
@Repository
public class MyProfileRepository extends ProfileRepository {

	@Autowired
	private DSLContext jooq;

	/**
	 * get the profile of the current user or null if no user available
	 * 
	 * @param keycloakId the ID of the user
	 * @return the profile
	 */
	public ProfileBean getProfile(String keycloakId) {
		return super.getProfile(jooq, keycloakId);
	}
}
