package de.ship.clip.modules.menu;

import java.util.List;
import java.util.Locale;

import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import de.ship.clip.lib.menu.MenuitemBean;
import de.ship.clip.lib.menu.MenuitemRepository;

/**
 * 
 * @author henkej
 *
 */
@Repository
public class MenuRepository extends MenuitemRepository {

	@Autowired
	private DSLContext jooq;

	/**
	 * get the menu items from the repository
	 * 
	 * @return the menu items
	 */
	public List<MenuitemBean> getMenuitems(Locale locale) {
		return super.getMenuitems(jooq, locale);
	}
}
