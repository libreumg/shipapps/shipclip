package de.ship.clip.modules.menu;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.bind.annotation.ModelAttribute;

import de.ship.clip.lib.menu.ListOfMenuitemBeansWrapper;
import de.ship.clip.lib.menu.MenuitemBean;
import de.ship.clip.modules.profile.ProfileController;

/**
 * 
 * @author henkej
 *
 */
public abstract class MenuController extends ProfileController {

	@Autowired
	private MenuRepository repository;
	
	@Value("${server.servlet.context-path}")
	private String appId;

	@Value("${stage:dev}")
	private String stage;

	public ListOfMenuitemBeansWrapper getWrapper(Locale locale) {
		return ListOfMenuitemBeansWrapper.of(getItems(locale));
	}

	public List<MenuitemBean> getItems(Locale locale) {
		return repository.getMenuitems(locale);
	}

	/**
	 * get the menu items
	 * 
	 * @return the menu items
	 */
	@ModelAttribute("menuitems")
	public List<MenuitemBean> getMenuitems() {
		List<MenuitemBean> list = getItems(LocaleContextHolder.getLocale());
		for (MenuitemBean bean : list) {
			if (bean.getAppId().equals(appId.replace("/", ""))) {
				bean.setCssClass(MenuitemBean.SELECTED_CLASS);
			}
		}
		return list;
	}

	/**
	 * get the stage value from the configuration
	 * 
	 * @return the stage value
	 */
	@ModelAttribute("stage")
	public String getStage() {
		return stage;
	}
}
