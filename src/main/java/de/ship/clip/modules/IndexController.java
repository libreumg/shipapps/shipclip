package de.ship.clip.modules;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import de.ship.clip.modules.menu.MenuController;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;

/**
 * 
 * @author henkej
 *
 */
@Controller
public class IndexController extends MenuController {

	@GetMapping("/")
	public String getRoot() {
		return "redirect:/index";
	}
	
	@GetMapping("/index")
	public String getIndex() {
		return "/index";
	}

	@GetMapping("/logout")
	public String getLogout(HttpServletRequest request) throws ServletException {
		request.logout();
		return "redirect:/";
	}
}
