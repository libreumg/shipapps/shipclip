package de.ship.clip.modules.theme;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import de.ship.clip.lib.common.ProfileHelper;
import de.ship.clip.modules.menu.MenuController;
import jakarta.servlet.http.HttpServletRequest;

/**
 * 
 * @author henkej
 * 
 */
@Controller
public class ThemeController extends MenuController {
	
	@Autowired
	private ThemeService service;
	
	@GetMapping(ProfileHelper.PROFILE_TOGGLE_THEME)
	public String toggleTheme(HttpServletRequest request) {
		service.toggleTheme(request.getUserPrincipal().getName());
		return "redirect:/";
	}
}
