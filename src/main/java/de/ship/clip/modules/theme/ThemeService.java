package de.ship.clip.modules.theme;

import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.ship.clip.lib.common.ProfileHelper;
import de.ship.clip.lib.profile.ProfileService;
import de.ship.clip.lib.profile.model.ProfileBean;
import de.ship.clip.modules.profile.MyProfileRepository;

/**
 * 
 * @author henkej
 * 
 */
@Service
public class ThemeService extends ProfileService {

	@Autowired
	private MyProfileRepository repository;

	@Autowired
	private DSLContext jooq;

	/**
	 * toggle the current profile's theme
	 * 
	 * @param keycloakID the keycloak ID
	 */
	public void toggleTheme(String keycloakID) {
		super.toggleProfile(repository, jooq, keycloakID);
	}

	/**
	 * get the theme of the current principal
	 * 
	 * @param keycloakID the keycloak ID
	 * 
	 * @return the theme or the default value light
	 */
	public String getTheme(String keycloakID) {
		ProfileBean bean = repository.getProfile(keycloakID);
		if (bean == null) {
			return ProfileHelper.PROFILE_THEME_LIGHT;
		} else {
			return bean.getTheme();
		}
	}
}
