package de.ship.clip.modules.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.ship.clip.common.KeycloakInformation;
import de.ship.clip.lib.menu.DisplayLabel;
import de.ship.clip.lib.menu.ListOfMenuitemBeansWrapper;
import de.ship.clip.lib.menu.MenuitemBean;
import de.ship.clip.modules.menu.MenuController;
import de.ship.clip.modules.theme.ThemeService;

/**
 * 
 * @author henkej
 *
 */
@RestController
public class RestClipController extends MenuController {

	@Autowired
	private KeycloakInformation keycloak;

	@Autowired
	private ThemeService themeService;

	/**
	 * provide the menu as a json object
	 * 
	 * @return the menu
	 */
	@RequestMapping(value = "/menu", produces = MediaType.APPLICATION_JSON_VALUE)
	public ListOfMenuitemBeansWrapper getMenu() {
		return getWrapper(LocaleContextHolder.getLocale());
	}

	/**
	 * provide the menu as a json object
	 * 
	 * @return the menu
	 */
	@CrossOrigin
	@RequestMapping(value = "/menu/{locale}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ListOfMenuitemBeansWrapper getMenu(@PathVariable String locale) {
		return getWrapper(Locale.forLanguageTag(locale));
	}

	/**
	 * get the options menu of the right side as json notation
	 * 
	 * @return the options menu
	 */
	@CrossOrigin
	@RequestMapping(value = "/options", produces = MediaType.APPLICATION_JSON_VALUE)
	public ListOfMenuitemBeansWrapper getOptions() {
		List<MenuitemBean> list = new ArrayList<>();
		list.add(MenuitemBean.of("x").withUrl(keycloak.getKeycloakUserUrl())
				.withLabel(DisplayLabel.ofTranslation("button.profile")));
		list.add(MenuitemBean.of("xx").withUrl(keycloak.getKeycloakLogoutUrl())
				.withLabel(DisplayLabel.ofTranslation("button.logout")));
		return ListOfMenuitemBeansWrapper.of(list);
	}

	/**
	 * get the theme of the current user
	 * 
	 * @return the theme or the default value light
	 */
	@RequestMapping(value = "/theme/{keycloakID}", produces = MediaType.TEXT_PLAIN_VALUE)
	public String getTheme(@PathVariable String keycloakID) {
		return themeService.getTheme(keycloakID);
	}
}
