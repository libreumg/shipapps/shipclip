package de.ship.clip.modules.admin.validation;

import static de.ship.clip.db.Tables.T_MENUITEM;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import de.ship.clip.modules.admin.AdminRepository;
import de.ship.clip.modules.admin.model.MenuitemBean;

/**
 * 
 * @author henkej
 * 
 */
public class UniqueUrlValidator extends UniqueValidator implements ConstraintValidator<UniqueUrl, MenuitemBean> {
	
	private String fieldCheck;
	
	@Autowired
	private AdminRepository repository;

	public void initialize(UniqueUrl uniqueUrl) {
		this.fieldCheck = uniqueUrl.fieldCheck();
	}
	
	@Override
	public boolean isValid(MenuitemBean bean, ConstraintValidatorContext context) {
		return super.isValid(bean, context, fieldCheck);
	}

	@Override
	public boolean checkValueExists(Integer pk, String check) {
		return repository.valueExists(pk, T_MENUITEM.URL.eq(check));
	}
}
