package de.ship.clip.modules.admin.model;

import java.io.Serializable;

/**
 * 
 * @author henkej
 * 
 */
public class JsonTranslationBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private String langkey;
	private String localecode;
	private String value;

	public static final JsonTranslationBean of(String langkey, String localecode, String value) {
		JsonTranslationBean bean = new JsonTranslationBean();
		bean.setLangkey(langkey);
		bean.setLocalecode(localecode);
		bean.setValue(value);
		return bean;
	}

	/**
	 * @return the langkey
	 */
	public String getLangkey() {
		return langkey;
	}

	/**
	 * @param langkey the langkey to set
	 */
	public void setLangkey(String langkey) {
		this.langkey = langkey;
	}

	/**
	 * @return the localecode
	 */
	public String getLocalecode() {
		return localecode;
	}

	/**
	 * @param localecode the localecode to set
	 */
	public void setLocalecode(String localecode) {
		this.localecode = localecode;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}
}
