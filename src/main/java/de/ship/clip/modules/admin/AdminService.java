package de.ship.clip.modules.admin;

import java.io.IOException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import de.ship.clip.modules.admin.model.DisplayLabelBean;
import de.ship.clip.modules.admin.model.JsonTranslationBean;
import de.ship.clip.modules.admin.model.MenuitemBean;
import de.ship.clip.modules.admin.model.TranslationBean;

/**
 * 
 * @author henkej
 *
 */
@Service
public class AdminService {
	@Autowired
	private AdminRepository repository;

	/**
	 * get all apps from the repository for maintaining them
	 * 
	 * @return all apps
	 */
	public List<MenuitemBean> getAllApps() {
		return repository.getAllApps();
	}

	public MenuitemBean getMenuitem(Integer id) {
		return repository.getMenuitem(id);
	}

	public void saveMenuitem(MenuitemBean bean) {
		repository.saveMenuitem(bean);
	}

	public void deleteMenuitem(Integer id) {
		repository.deleteMenuitem(id);
	}

	public DisplayLabelBean getDisplayLabel(Integer id, Boolean description) {
		MenuitemBean bean = repository.getMenuitem(id);
		return description ? bean.getDescription() : bean.getLabel();
	}

	public void addDisplayLabel(Integer id, Boolean description) {
		repository.addDisplayLabel(id, description);
	}

	public void saveDisplayLabel(DisplayLabelBean bean) {
		if (bean != null) {
			if (bean.getLabel() != null && bean.getLabel().isBlank()) {
				bean.setLabel(null);
			}
			if (bean.getLangkey() != null && bean.getLangkey().isBlank()) {
				bean.setLangkey(null);
			}
		}
		repository.updateDisplayLabel(bean);
	}

	public List<TranslationBean> getAllTranslations() {
		return repository.getAllTranslations();
	}

	public void saveTranslation(TranslationBean bean) {
		repository.upsertTranslation(bean);
	}

	public void deleteTranslation(Integer id) {
		repository.deleteTranslation(id);
	}

	public TranslationBean getTranslation(Integer id) {
		return repository.getTranslation(id);
	}

	public String getTranslationExport() {
		return new GsonBuilder().create().toJson(repository.getTranslationsAsJson());
	}

	public void handleUploadFile(MultipartFile file) throws IOException, JsonSyntaxException {
		String json = new String(file.getBytes());
		List<JsonTranslationBean> list = new GsonBuilder().create().fromJson(json,
				new TypeToken<List<JsonTranslationBean>>() {
				}.getType());
		repository.upsertTranslations(list);
	}
}
