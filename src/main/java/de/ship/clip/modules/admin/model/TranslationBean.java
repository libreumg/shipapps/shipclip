package de.ship.clip.modules.admin.model;

import java.io.Serializable;

import de.ship.clip.db.enums.EnumLocale;

/**
 * 
 * @author henkej
 *
 */
public class TranslationBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer pk;
	private String langkey;
	private EnumLocale localecode;
	private String value;

	public static final TranslationBean of(Integer pk, String langkey, EnumLocale localecode, String value) {
		TranslationBean bean = new TranslationBean();
		bean.setPk(pk);
		bean.setLangkey(langkey);
		bean.setLocalecode(localecode);
		bean.setValue(value);
		return bean;
	}

	/**
	 * @return the langkey
	 */
	public String getLangkey() {
		return langkey;
	}

	/**
	 * @param langkey the langkey to set
	 */
	public void setLangkey(String langkey) {
		this.langkey = langkey;
	}

	/**
	 * @return the pk
	 */
	public Integer getPk() {
		return pk;
	}

	/**
	 * @param pk the pk to set
	 */
	public void setPk(Integer pk) {
		this.pk = pk;
	}

	/**
	 * @return the localecode
	 */
	public EnumLocale getLocalecode() {
		return localecode;
	}

	/**
	 * @param localecode the localecode to set
	 */
	public void setLocalecode(EnumLocale localecode) {
		this.localecode = localecode;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

}
