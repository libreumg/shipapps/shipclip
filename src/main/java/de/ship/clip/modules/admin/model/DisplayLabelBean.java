package de.ship.clip.modules.admin.model;

import java.io.Serializable;

/**
 * 
 * @author henkej
 *
 */
public class DisplayLabelBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer pk;
	private String label; 
	private String langkey;
	private TranslationBundleBean translation;
	
	public static final DisplayLabelBean of(Integer pk, String label, String langkey, TranslationBundleBean translation) {
		DisplayLabelBean bean = new DisplayLabelBean();
		bean.setPk(pk);
		bean.setLabel(label);
		bean.setLangkey(langkey);
		bean.setTranslation(translation);
		return bean;
	}
	
	/**
	 * get the current label; first, find the translation, second, find the label; if all not found, return the langkey
	 * 
	 * @return the current label
	 */
	public String getCurrentLabel(String locale) {
		if (translation != null) {
			return translation.getTranslation(locale);
		} else if (label != null) {
			return label;
		} else {
			return langkey;
		}
	}

	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * @param label the label to set
	 */
	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * @return the translation
	 */
	public TranslationBundleBean getTranslation() {
		return translation;
	}

	/**
	 * @param translation the translation to set
	 */
	public void setTranslation(TranslationBundleBean translation) {
		this.translation = translation;
	}

	/**
	 * @return the langkey
	 */
	public String getLangkey() {
		return langkey;
	}

	/**
	 * @param langkey the langkey to set
	 */
	public void setLangkey(String langkey) {
		this.langkey = langkey;
	}

	/**
	 * @return the pk
	 */
	public Integer getPk() {
		return pk;
	}

	/**
	 * @param pk the pk to set
	 */
	public void setPk(Integer pk) {
		this.pk = pk;
	}
}
