package de.ship.clip.modules.admin;

import java.io.IOException;
import java.security.Principal;
import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.JsonSyntaxException;

import de.ship.clip.db.enums.EnumLocale;
import de.ship.clip.modules.admin.model.DisplayLabelBean;
import de.ship.clip.modules.admin.model.MenuitemBean;
import de.ship.clip.modules.admin.model.TranslationBean;
import de.ship.clip.modules.menu.MenuController;
import jakarta.validation.Valid;

/**
 * 
 * @author henkej
 *
 */
@Controller
public class AdminController extends MenuController {

	private static final Logger LOGGER = LogManager.getLogger(AdminController.class);

	@Autowired
	private AdminService service;

	@GetMapping("/admin")
	public String getAdmin(final Model model, Principal principal) {
		model.addAttribute("currentUser", principal.getName());
		model.addAttribute("menuitemList", service.getAllApps());
		return "/admin/dashboard";
	}

	@GetMapping("/admin/menuitem/{id}")
	public String loadMenuitem(@PathVariable Integer id, final Model model) {
		model.addAttribute("bean", service.getMenuitem(id));
		return "/admin/menuitem";
	}

	@PostMapping("/admin/menuitem/save")
	public String saveMenuitem(@ModelAttribute("bean") @Valid MenuitemBean bean, BindingResult bindingResult,
			final Model model) {
		if (bindingResult.hasErrors()) {
			return "/admin/menuitem";
		}
		service.saveMenuitem(bean);
		return "redirect:/admin";
	}

	@GetMapping("/admin/menuitem/{id}/label/edit")
	public String prepareEditFormOfLabel(@PathVariable Integer id, final Model model) {
		model.addAttribute("menuitemid", id);
		model.addAttribute("bean", service.getDisplayLabel(id, false));
		return "/admin/displaylabel";
	}

	@GetMapping("/admin/menuitem/{id}/label/add")
	public String prepareAddFormOfLabel(@PathVariable Integer id, final Model model) {
		model.addAttribute("menuitemid", id);
		service.addDisplayLabel(id, false);
		model.addAttribute("bean", service.getDisplayLabel(id, false));
		return "/admin/displaylabel";
	}

	@GetMapping("/admin/menuitem/{id}/description/edit")
	public String prepareEditFormOfDescription(@PathVariable Integer id, final Model model) {
		model.addAttribute("menuitemid", id);
		model.addAttribute("bean", service.getDisplayLabel(id, true));
		return "/admin/displaylabel";
	}

	@GetMapping("/admin/menuitem/{id}/description/add")
	public String prepareAddFormOfDescription(@PathVariable Integer id, final Model model) {
		model.addAttribute("menuitemid", id);
		service.addDisplayLabel(id, true);
		model.addAttribute("bean", service.getDisplayLabel(id, true));
		return "/admin/displaylabel";
	}

	@GetMapping("/admin/addmenuitem")
	public String prepareAddForm(final Model model) {
		model.addAttribute("bean", new MenuitemBean());
		return "/admin/menuitem";
	}

	@GetMapping("/admin/deletemenuitem/{id}")
	public String deleteMenuItem(@PathVariable Integer id) {
		service.deleteMenuitem(id);
		return "redirect:/admin";
	}

	@PostMapping("/admin/menuitem/{id}/displaylabel/save")
	public String saveDisplayLabel(@PathVariable Integer id, @ModelAttribute DisplayLabelBean bean,
			BindingResult bindingResult) {
		service.saveDisplayLabel(bean);
		return "redirect:/admin/menuitem/" + id;
	}

	@GetMapping("/admin/translationlist")
	public String loadTranslations(final Model model) {
		try {
			model.addAttribute("list", service.getAllTranslations());
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			model.addAttribute("list", new ArrayList<TranslationBean>());
			model.addAttribute("errorMessage", e.getMessage());
			model.addAttribute("errorMessageText", e.getStackTrace().toString());
		}
		return "/admin/translationlist";
	}

	@GetMapping("/admin/addtranslation")
	public String prepareTranslationMask(final Model model) {
		model.addAttribute("bean", new TranslationBean());
		model.addAttribute("locales", EnumLocale.values());
		return "/admin/translation";
	}

	@GetMapping("/admin/translation/{id}")
	public String prepareEditTranslation(@PathVariable Integer id, final Model model) {
		model.addAttribute("bean", service.getTranslation(id));
		model.addAttribute("locales", EnumLocale.values());
		return "/admin/translation";
	}

	@PostMapping("/admin/translation/save")
	public String saveTranslation(@ModelAttribute TranslationBean bean, BindingResult bindingResult) {
		service.saveTranslation(bean);
		return "redirect:/admin/translationlist";
	}

	@GetMapping("/admin/deletetranslation/{id}")
	public String deleteTranslation(@PathVariable Integer id) {
		service.deleteTranslation(id);
		return "redirect:/admin/translationlist";
	}

	@RequestMapping(path = "/admin/translation/export", method = RequestMethod.GET)
	public ResponseEntity<String> exportTranslation() {
		return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(service.getTranslationExport());
	}

	@PostMapping("/admin/translation/upload")
	public String handleUpload(@RequestParam("file") MultipartFile file) {
		try {
			service.handleUploadFile(file);
		} catch (IOException|JsonSyntaxException e) {
			LOGGER.error(e.getMessage(), e);
			// TODO: inform the user by a redirect error message
		}
		return "redirect:/admin/translationlist";
	}
}
