package de.ship.clip.modules.admin.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

/**
 * 
 * @author henkej
 *
 */
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = UniqueAppIdValidator.class)
@Documented
public @interface UniqueAppId {
	String message() default "{validation.not.unique}";

	String fieldCheck();
	
	Class<?>[] groups() default {};
	
	Class<? extends Payload>[] payload() default {};
}
