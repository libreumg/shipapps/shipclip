package de.ship.clip.modules.admin;

import static de.ship.clip.db.Tables.T_DISPLAYLABEL;
import static de.ship.clip.db.Tables.T_MENUITEM;
import static de.ship.clip.db.Tables.T_TRANSLATION;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.DeleteConditionStep;
import org.jooq.InsertOnDuplicateSetMoreStep;
import org.jooq.InsertOnDuplicateStep;
import org.jooq.InsertResultStep;
import org.jooq.SelectConditionStep;
import org.jooq.SelectWhereStep;
import org.jooq.UpdateConditionStep;
import org.jooq.impl.DSL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import de.ship.clip.db.enums.EnumLocale;
import de.ship.clip.db.tables.records.TDisplaylabelRecord;
import de.ship.clip.db.tables.records.TMenuitemRecord;
import de.ship.clip.db.tables.records.TTranslationRecord;
import de.ship.clip.modules.admin.model.DisplayLabelBean;
import de.ship.clip.modules.admin.model.JsonTranslationBean;
import de.ship.clip.modules.admin.model.MenuitemBean;
import de.ship.clip.modules.admin.model.TranslationBean;
import de.ship.clip.modules.admin.model.TranslationBundleBean;

/**
 * 
 * @author henkej
 *
 */
@Repository
public class AdminRepository {
	private static final Logger LOGGER = LogManager.getLogger(AdminRepository.class);

	@Autowired
	private DSLContext jooq;

	/**
	 * get all apps from the database
	 * 
	 * @return all apps
	 */
	public List<MenuitemBean> getAllApps() {
		SelectWhereStep<TTranslationRecord> sql0 = jooq.selectFrom(T_TRANSLATION);
		LOGGER.trace(sql0.toString());
		Map<String, TranslationBundleBean> translationsWrapper = new HashMap<>();
		for (TTranslationRecord r : sql0.fetch()) {
			TranslationBundleBean bean = translationsWrapper.get(r.getLangkey());
			if (bean == null) {
				bean = TranslationBundleBean.of(r.getLangkey(), new ArrayList<>());
				translationsWrapper.put(r.getLangkey(), bean);
			}
			bean.getMap().put(r.getLocalecode(),
					TranslationBean.of(r.getPk(), r.getLangkey(), r.getLocalecode(), r.getValue()));
		}

		SelectWhereStep<TDisplaylabelRecord> sql1 = jooq.selectFrom(T_DISPLAYLABEL);
		LOGGER.trace(sql1.toString());
		Map<Integer, DisplayLabelBean> displaylabelsWrapper = new HashMap<>();
		for (TDisplaylabelRecord r : sql1.fetch()) {
			displaylabelsWrapper.put(r.getPk(),
					DisplayLabelBean.of(r.getPk(), r.getLabel(), r.getLangkey(), translationsWrapper.get(r.getLangkey())));
		}

		SelectWhereStep<TMenuitemRecord> sql2 = jooq.selectFrom(T_MENUITEM);
		LOGGER.trace(sql2.toString());
		List<MenuitemBean> menuitems = new ArrayList<>();
		for (TMenuitemRecord r : sql2.fetch()) {
			menuitems.add(MenuitemBean.of(r.getPk(), r.getUrl(), r.getIconUrl(), displaylabelsWrapper.get(r.getFkLabel()),
					displaylabelsWrapper.get(r.getFkDescription()), r.getAppId(), r.getOrderNr(), r.getKeycloakRole()));
		}

		return menuitems;
	}

	public MenuitemBean getMenuitem(Integer id) {
		SelectWhereStep<TTranslationRecord> sql0 = jooq.selectFrom(T_TRANSLATION);
		LOGGER.trace(sql0.toString());
		Map<String, TranslationBundleBean> translationsWrapper = new HashMap<>();
		for (TTranslationRecord r : sql0.fetch()) {
			TranslationBundleBean bean = translationsWrapper.get(r.getLangkey());
			if (bean == null) {
				bean = TranslationBundleBean.of(r.getLangkey(), new ArrayList<>());
				translationsWrapper.put(r.getLangkey(), bean);
			}
			bean.getMap().put(r.getLocalecode(),
					TranslationBean.of(r.getPk(), r.getLangkey(), r.getLocalecode(), r.getValue()));
		}

		SelectWhereStep<TDisplaylabelRecord> sql1 = jooq.selectFrom(T_DISPLAYLABEL);
		LOGGER.trace(sql1.toString());
		Map<Integer, DisplayLabelBean> displaylabelsWrapper = new HashMap<>();
		for (TDisplaylabelRecord r : sql1.fetch()) {
			displaylabelsWrapper.put(r.getPk(),
					DisplayLabelBean.of(r.getPk(), r.getLabel(), r.getLangkey(), translationsWrapper.get(r.getLangkey())));
		}

		SelectConditionStep<TMenuitemRecord> sql = jooq.selectFrom(T_MENUITEM).where(T_MENUITEM.PK.eq(id));
		LOGGER.trace(sql.toString());
		TMenuitemRecord r = sql.fetchOne();

		if (r != null) {
			return MenuitemBean.of(r.getPk(), r.getUrl(), r.getIconUrl(), displaylabelsWrapper.get(r.getFkLabel()),
					displaylabelsWrapper.get(r.getFkDescription()), r.getAppId(), r.getOrderNr(), r.getKeycloakRole());
		} else {
			return null;
		}
	}

	public void saveMenuitem(MenuitemBean bean) {
		jooq.transaction(t -> {
			if (bean.getPk() != null) {
				UpdateConditionStep<TMenuitemRecord> sql = DSL.using(t)
				// @formatter:off
			  	.update(T_MENUITEM)
			  	.set(T_MENUITEM.URL, bean.getUrl())
			  	.set(T_MENUITEM.ICON_URL, bean.getIconUrl())
			  	.set(T_MENUITEM.APP_ID, bean.getAppId())
			  	.set(T_MENUITEM.FK_LABEL, bean.getLabelPk())
			  	.set(T_MENUITEM.FK_DESCRIPTION, bean.getDescriptionPk())
			  	.set(T_MENUITEM.ORDER_NR, bean.getOrderNr())
			  	.set(T_MENUITEM.KEYCLOAK_ROLE, bean.getKeycloakRole())
			  	.where(T_MENUITEM.PK.eq(bean.getPk()));
				// @formatter:on
				LOGGER.trace(sql.toString());
				sql.execute();
			} else {
				InsertResultStep<TDisplaylabelRecord> sql1 = DSL.using(t)
				// @formatter:off
					.insertInto(T_DISPLAYLABEL, 
							        T_DISPLAYLABEL.LABEL)
					.values(bean.getAppId())
					.returning(T_DISPLAYLABEL.PK);
				// @formatter:on
				LOGGER.trace(sql1.toString());
				Integer fkLabel = sql1.fetchOne().get(T_DISPLAYLABEL.PK);

				InsertOnDuplicateStep<TMenuitemRecord> sql2 = DSL.using(t)
				// @formatter:off
					.insertInto(T_MENUITEM, 
											T_MENUITEM.URL, 
											T_MENUITEM.APP_ID, 
											T_MENUITEM.FK_LABEL,
											T_MENUITEM.ORDER_NR,
											T_MENUITEM.KEYCLOAK_ROLE)				
					.select(DSL.using(t)
						.select(DSL.value(bean.getUrl()),
										DSL.value(bean.getAppId()),
										DSL.value(fkLabel),
								    DSL.coalesce(DSL.max(T_MENUITEM.ORDER_NR), 1).div(10).times(10).plus(10),
								    DSL.value(bean.getKeycloakRole()))
						.from(T_MENUITEM));
				// @formatter:on
				LOGGER.trace(sql2.toString());
				sql2.execute();
			}
		});
	}

	/**
	 * delete the menu item from the database
	 * 
	 * @param id the ID of the menuitem
	 */
	public void deleteMenuitem(Integer id) {
		DeleteConditionStep<TMenuitemRecord> sql = jooq.deleteFrom(T_MENUITEM).where(T_MENUITEM.PK.eq(id));
		LOGGER.trace(sql.toString());
		sql.execute();
	}

	/**
	 * create a new display label
	 * 
	 * @param id          the ID of the menu item
	 * @param description if true, connect it to fkDescription; if false, connect it
	 *                    to fkLabel
	 */
	public void addDisplayLabel(Integer id, Boolean description) {
		jooq.transaction(t -> {
			InsertResultStep<TDisplaylabelRecord> sql = DSL.using(t)
			// @formatter:off
			  .insertInto(T_DISPLAYLABEL,
				  	        T_DISPLAYLABEL.LABEL)
			  .values("")
			  .returning(T_DISPLAYLABEL.PK);
			// @formatter:on
			LOGGER.trace(sql.toString());
			Integer fkDisplayLabel = sql.fetchOne().get(T_DISPLAYLABEL.PK);
			UpdateConditionStep<TMenuitemRecord> sql2 = DSL.using(t)
			// @formatter:off
				.update(T_MENUITEM)
				.set(description ? T_MENUITEM.FK_DESCRIPTION : T_MENUITEM.FK_LABEL, fkDisplayLabel)
				.where(T_MENUITEM.PK.eq(id));
			// @formatter:on
			LOGGER.trace(sql2.toString());
			sql2.execute();
		});
	}

	/**
	 * updates the display label bean
	 * 
	 * @param bean the bean
	 */
	public void updateDisplayLabel(DisplayLabelBean bean) {
		UpdateConditionStep<TDisplaylabelRecord> sql = jooq
		// @formatter:off
			.update(T_DISPLAYLABEL)
			.set(T_DISPLAYLABEL.LABEL, bean.getLabel())
			.set(T_DISPLAYLABEL.LANGKEY, bean.getLangkey())
			.where(T_DISPLAYLABEL.PK.eq(bean.getPk()));
		// @formatter:on
		LOGGER.trace(sql.toString());
		sql.execute();
	}

	/**
	 * get all translations from the database
	 * 
	 * @return the list of found translations; an empty one at least
	 */
	public List<TranslationBean> getAllTranslations() {
		SelectWhereStep<TTranslationRecord> sql = jooq.selectFrom(T_TRANSLATION);
		LOGGER.trace(sql.toString());
		List<TranslationBean> list = new ArrayList<>();
		for (TTranslationRecord r : sql.fetch()) {
			list.add(TranslationBean.of(r.getPk(), r.getLangkey(), r.getLocalecode(), r.getValue()));
		}
		return list;
	}

	/**
	 * upsert the translation in the bean
	 * 
	 * @param bean the bean
	 */
	public void upsertTranslation(TranslationBean bean) {
		InsertOnDuplicateSetMoreStep<TTranslationRecord> sql = jooq
		// @formatter:off
			.insertInto(T_TRANSLATION,
					        T_TRANSLATION.LANGKEY,
					        T_TRANSLATION.LOCALECODE,
					        T_TRANSLATION.VALUE)
			.values(bean.getLangkey(), bean.getLocalecode(), bean.getValue())
			.onConflict(T_TRANSLATION.LANGKEY, T_TRANSLATION.LOCALECODE)
			.doUpdate()
			.set(T_TRANSLATION.VALUE, bean.getValue());
		// @formatter:on
		LOGGER.trace(sql.toString());
		sql.execute();
	}

	/**
	 * delete translation with id from the database
	 * 
	 * @param id the ID
	 */
	public void deleteTranslation(Integer id) {
		DeleteConditionStep<TTranslationRecord> sql = jooq.deleteFrom(T_TRANSLATION).where(T_TRANSLATION.PK.eq(id));
		LOGGER.trace(sql.toString());
		sql.execute();
	}

	/**
	 * get the translation of this ID
	 * 
	 * @param id the ID
	 * @return the translation bean or null if not found
	 */
	public TranslationBean getTranslation(Integer id) {
		SelectConditionStep<TTranslationRecord> sql = jooq.selectFrom(T_TRANSLATION).where(T_TRANSLATION.PK.eq(id));
		LOGGER.trace(sql.toString());
		TTranslationRecord r = sql.fetchOne();
		if (r != null) {
			return TranslationBean.of(r.getPk(), r.getLangkey(), r.getLocalecode(), r.getValue());
		} else {
			return null;
		}
	}

	/**
	 * check if the amount of values is > 0 for the condition cond; exclude the data
	 * set with the pk
	 * 
	 * @param pk  the ID of the data to be excluded; if null, pk is ignored
	 * @param url the url
	 * @return true or false
	 */
	public boolean valueExists(Integer pk, Condition cond) {
		cond = pk == null ? cond : cond.and(T_MENUITEM.PK.ne(pk));
		SelectConditionStep<TMenuitemRecord> sql = jooq.selectFrom(T_MENUITEM).where(cond);
		LOGGER.trace(sql.toString());
		return sql.fetch().size() > 0;
	}

	/**
	 * get all translations as a json string
	 * 
	 * @return the translations
	 */
	public List<JsonTranslationBean> getTranslationsAsJson() {
		SelectWhereStep<TTranslationRecord> sql = jooq.selectFrom(T_TRANSLATION);
		LOGGER.trace(sql.toString());
		List<JsonTranslationBean> list = new ArrayList<>();
		for (TTranslationRecord r : sql.fetch()) {
			String langkey = r.getLangkey();
			EnumLocale localeCode = r.getLocalecode();
			String value = r.getValue();
			list.add(JsonTranslationBean.of(langkey, localeCode == null ? null : localeCode.getLiteral(), value));
		}
		return list;
	}

	/**
	 * upsert all beans in the list
	 * 
	 * @param list the list
	 */
	public void upsertTranslations(List<JsonTranslationBean> list) {
		jooq.transaction(t -> {
			for (JsonTranslationBean bean : list) {
				EnumLocale localeCode = EnumLocale.lookupLiteral(bean.getLocalecode());
				InsertOnDuplicateSetMoreStep<TTranslationRecord> sql = jooq
				// @formatter:off
					.insertInto(T_TRANSLATION,
							        T_TRANSLATION.LANGKEY,
							        T_TRANSLATION.LOCALECODE,
							        T_TRANSLATION.VALUE)
					.values(bean.getLangkey(), localeCode, bean.getValue())
					.onConflict(T_TRANSLATION.LANGKEY, T_TRANSLATION.LOCALECODE)
					.doUpdate()
					.set(T_TRANSLATION.VALUE, bean.getValue());
				// @formatter:on
				LOGGER.trace(sql.toString());
				sql.execute();
			}
		});
	}
}
