package de.ship.clip.modules.admin.model;

import java.io.Serializable;

import jakarta.validation.constraints.NotBlank;

import de.ship.clip.modules.admin.validation.UniqueAppId;
import de.ship.clip.modules.admin.validation.UniqueOrderNr;
import de.ship.clip.modules.admin.validation.UniqueUrl;
import groovyjarjarantlr4.v4.runtime.misc.NotNull;

/**
 * 
 * @author henkej
 *
 */
@UniqueAppId(fieldCheck = "appId")
@UniqueUrl(fieldCheck = "url")
@UniqueOrderNr(fieldCheck = "orderNr")
public class MenuitemBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer pk;
	@NotBlank
	private String url;
	private String iconUrl;
	private DisplayLabelBean label;
	private DisplayLabelBean description;
	@NotBlank
	private String appId;
	@NotNull
	private Integer orderNr;
	private String keycloakRole;

	public static final MenuitemBean of(Integer pk, String url, String iconUrl, DisplayLabelBean label,
			DisplayLabelBean description, String appId, Integer orderNr, String keycloakRole) {
		MenuitemBean bean = new MenuitemBean();
		bean.setPk(pk);
		bean.setUrl(url);
		bean.setIconUrl(iconUrl);
		bean.setLabel(label);
		bean.setDescription(description);
		bean.setAppId(appId);
		bean.setOrderNr(orderNr);
		bean.setKeycloakRole(keycloakRole);
		return bean;
	}

	/**
	 * get the pk of the label entry
	 * 
	 * @return the pk or null
	 */
	public Integer getLabelPk() {
		return label == null ? null : label.getPk();
	}

	/**
	 * get the pk of the description entry
	 * 
	 * @return the pk or null
	 */
	public Integer getDescriptionPk() {
		return description == null ? null : description.getPk();
	}

	/**
	 * @return the pk
	 */
	public Integer getPk() {
		return pk;
	}

	/**
	 * @param pk the pk to set
	 */
	public void setPk(Integer pk) {
		this.pk = pk;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the label
	 */
	public DisplayLabelBean getLabel() {
		return label;
	}

	/**
	 * @param label the label to set
	 */
	public void setLabel(DisplayLabelBean label) {
		this.label = label;
	}

	/**
	 * @return the description
	 */
	public DisplayLabelBean getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(DisplayLabelBean description) {
		this.description = description;
	}

	/**
	 * @return the appId
	 */
	public String getAppId() {
		return appId;
	}

	/**
	 * @param appId the appId to set
	 */
	public void setAppId(String appId) {
		this.appId = appId;
	}

	/**
	 * @return the iconUrl
	 */
	public String getIconUrl() {
		return iconUrl;
	}

	/**
	 * @param iconUrl the iconUrl to set
	 */
	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}

	/**
	 * @return the orderNr
	 */
	public Integer getOrderNr() {
		return orderNr;
	}

	/**
	 * @param orderNr the orderNr to set
	 */
	public void setOrderNr(Integer orderNr) {
		this.orderNr = orderNr;
	}

	/**
	 * @return the keycloakRole
	 */
	public String getKeycloakRole() {
		return keycloakRole;
	}

	/**
	 * @param keycloakRole the keycloakRole to set
	 */
	public void setKeycloakRole(String keycloakRole) {
		this.keycloakRole = keycloakRole;
	}
}
