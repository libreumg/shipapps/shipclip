package de.ship.clip.modules.admin;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import de.ship.clip.db.tables.records.TDisplaylabelRecord;
import de.ship.clip.db.tables.records.TTranslationRecord;
import de.ship.clip.modules.admin.model.MenuitemBean;

/**
 * 
 * @author henkej
 *
 */
public class ConfigWrapperBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private List<MenuitemBean> menuitems;
	private List<TDisplaylabelRecord> displaylabels;
	private List<TTranslationRecord> translations;

	private ConfigWrapperBean() {
		menuitems = new ArrayList<>();
		displaylabels = new ArrayList<>();
		translations = new ArrayList<>();
	}

	public static final ConfigWrapperBean of(List<MenuitemBean> menuitems, List<TDisplaylabelRecord> displaylabels,
			List<TTranslationRecord> translations) {
		ConfigWrapperBean bean = new ConfigWrapperBean();
		bean.getMenuitems().addAll(menuitems);
		bean.getDisplaylabels().addAll(displaylabels);
		bean.getTranslations().addAll(translations);
		return bean;
	}

	/**
	 * @return the menuitems
	 */
	public List<MenuitemBean> getMenuitems() {
		return menuitems;
	}

	/**
	 * @return the displaylabels
	 */
	public List<TDisplaylabelRecord> getDisplaylabels() {
		return displaylabels;
	}

	/**
	 * @return the translations
	 */
	public List<TTranslationRecord> getTranslations() {
		return translations;
	}
}
