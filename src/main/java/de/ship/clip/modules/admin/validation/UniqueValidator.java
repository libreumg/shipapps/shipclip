package de.ship.clip.modules.admin.validation;

import jakarta.validation.ConstraintValidatorContext;

import org.jooq.tools.StringUtils;
import org.springframework.beans.BeanWrapperImpl;

import de.ship.clip.modules.admin.model.MenuitemBean;

/**
 * 
 * @author henkej
 * 
 */
public abstract class UniqueValidator {

	/**
	 * check if the field check is multiple times in the table; exclude pk from the
	 * check
	 * 
	 * @param pk    the ID of the data set
	 * @param check the value to check
	 * @return true or false
	 */
	public abstract boolean checkValueExists(Integer pk, String check);

	/**
	 * checks if the field check is multiple times in the table; exclude pk from the
	 * check
	 * 
	 * @param bean       the bean
	 * @param context    the context
	 * @param fieldCheck the check field
	 * @return true or false
	 */
	public boolean isValid(MenuitemBean bean, ConstraintValidatorContext context, String fieldCheck) {
		Object valueCheck = new BeanWrapperImpl(bean).getPropertyValue(fieldCheck);
		final String check = (String) valueCheck;
		final Integer pk = bean.getPk();
		if (StringUtils.isBlank(check)) {
			// is validated by @NotBlank
			return true;
		} else if (checkValueExists(pk, check)) {
			context.disableDefaultConstraintViolation();
			context.buildConstraintViolationWithTemplate(context.getDefaultConstraintMessageTemplate())
					.addPropertyNode(fieldCheck).addConstraintViolation();
			return false;
		} else {
			return true;
		}
	}
}
