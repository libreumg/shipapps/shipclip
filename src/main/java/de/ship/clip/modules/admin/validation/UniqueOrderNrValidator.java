package de.ship.clip.modules.admin.validation;

import static de.ship.clip.db.Tables.T_MENUITEM;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import org.springframework.beans.BeanWrapperImpl;
import org.springframework.beans.factory.annotation.Autowired;

import de.ship.clip.modules.admin.AdminRepository;
import de.ship.clip.modules.admin.model.MenuitemBean;

/**
 * 
 * @author henkej
 * 
 */
public class UniqueOrderNrValidator implements ConstraintValidator<UniqueOrderNr, MenuitemBean> {

	private String fieldCheck;
	
	@Autowired
	private AdminRepository repository;

	public void initialize(UniqueOrderNr uniqueOrderNr) {
		this.fieldCheck = uniqueOrderNr.fieldCheck();
	}
	
	@Override
	public boolean isValid(MenuitemBean bean, ConstraintValidatorContext context) {
		Object valueCheck = new BeanWrapperImpl(bean).getPropertyValue(fieldCheck);
		final Integer check = (Integer) valueCheck;
		final Integer pk = bean.getPk();
		if (checkValueExists(pk, check)) {
			context.disableDefaultConstraintViolation();
			context.buildConstraintViolationWithTemplate(context.getDefaultConstraintMessageTemplate())
					.addPropertyNode(fieldCheck).addConstraintViolation();
			return false;
		} else {
			return true;
		}
	}
	
	public boolean checkValueExists(Integer pk, Integer check) {
		return repository.valueExists(pk, T_MENUITEM.ORDER_NR.eq(check));
	}
}
