package de.ship.clip.modules.admin.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.ship.clip.db.enums.EnumLocale;

/**
 * 
 * @author henkej
 *
 */
public class TranslationBundleBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private String langkey;
	private Map<EnumLocale, TranslationBean> map;

	public static final TranslationBundleBean of(String langkey, List<TranslationBean> translations) {
		TranslationBundleBean bean = new TranslationBundleBean();
		bean.setLangkey(langkey);
		bean.setMap(new HashMap<>());
		for (TranslationBean t : translations) {
			bean.getMap().put(t.getLocalecode(), t);
		}
		return bean;
	}

	/**
	 * get the translation of the locale
	 * 
	 * @param locale the locale
	 * @return the translation or null if not available
	 */
	public String getTranslation(String locale) {
		EnumLocale l = EnumLocale.lookupLiteral(locale);
		TranslationBean bean = map.get(l);
		return bean == null ? null : bean.getValue();
	}

	/**
	 * @return the langkey
	 */
	public String getLangkey() {
		return langkey;
	}

	/**
	 * @param langkey the langkey to set
	 */
	public void setLangkey(String langkey) {
		this.langkey = langkey;
	}

	/**
	 * @return the map
	 */
	public Map<EnumLocale, TranslationBean> getMap() {
		return map;
	}

	/**
	 * @param map the map to set
	 */
	public void setMap(Map<EnumLocale, TranslationBean> map) {
		this.map = map;
	}
}
