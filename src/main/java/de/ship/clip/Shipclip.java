package de.ship.clip;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

/**
 * 
 * @author henkej
 *
 */
@SpringBootApplication
public class Shipclip extends SpringBootServletInitializer {

  @Override
  protected SpringApplicationBuilder configure(
    SpringApplicationBuilder application) {
    return application.sources(Shipclip.class);
  }
  
	public static void main(String[] args) {
		SpringApplication.run(Shipclip.class, args);
	}
}
