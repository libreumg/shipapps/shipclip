package de.ship.clip.configuration;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.SQLDialect;
import org.jooq.impl.DataSourceConnectionProvider;
import org.jooq.impl.DefaultConfiguration;
import org.jooq.impl.DefaultDSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.TransactionAwareDataSourceProxy;

import javax.sql.DataSource;

/**
 * 
 * @author henkej
 *
 */
@Configuration
public class DatabaseConfiguration {
	private static final Logger LOGGER = LogManager.getLogger(DatabaseConfiguration.class);

	@Autowired
	private DataSource dataSource;
	
	@Value("${jooq.dialect:POSTGRES}")
	private String jooqDialect;

	@Bean
	public DataSourceConnectionProvider connectionProvider() {
		return new DataSourceConnectionProvider(new TransactionAwareDataSourceProxy(dataSource));
	}
	
	@Bean
	public DefaultDSLContext dsl() {
		return new DefaultDSLContext(configuration());
	}

	@Bean
	public void disableLogo() {
		System.setProperty("org.jooq.no-logo", "true");
	}

	public DefaultConfiguration configuration() {
		DefaultConfiguration jooqConfiguration = new DefaultConfiguration();
		jooqConfiguration.set(connectionProvider());
		jooqConfiguration.set(SQLDialect.valueOf(jooqDialect));
		LOGGER.debug("try to use the jooq driver of {}", jooqDialect);
		return jooqConfiguration;
	}
}
