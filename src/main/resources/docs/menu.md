The menu endpoint can be achieved by simply adding `/menu/` and the locale (one of `de`, `en`, `es`, `it`, `pl`, `pt`, `ro`) to a running instance of `Shipclip`'s URL, e.g. `/menu/en` for the English one.

The resulting json string is structured and contains javascript objects as show in the example, each label and description field in the corresponding language:

```json
{
	"list": [{
		"appId": "myAwesomeApp1",
		"url": "https://my.awesome.url",
    "icon_url": "http://my.awesome.icon",
		"label": {
			"langkey": "a.common.name"
		},
		"description": {
			"name": "a description"
		}
	}, {
		"appId": "myAwesomeApp2",
		"url": "https://another.awesome.url",
    "icon_url": "http://any.icon.url",
		"label": {
			"name": "another name"
		},
		"description": {
			"langkey": "a.common.description"
		}
	}],
	"version": 2
}
```

Note: the `appId` equals the rule defined in keycloak for the corresponding client.
